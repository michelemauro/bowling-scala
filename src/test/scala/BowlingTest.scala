import org.scalatest.prop.TableDrivenPropertyChecks._
import org.scalatest.Matchers._
import org.scalatest.PropSpec
import scala.language.postfixOps
import org.scalatest.Matchers
import org.scalatest.prop.TableFor2

class BowlingTest extends PropSpec with Matchers {

  def score(t: TableFor2[List[Int], Int]) =
    forAll(t) { (input: List[Int], expected: Int) =>
      // println("==== " + input)
      new BowlingGame().score(input) should be(expected)
    }

  val simplePins = Table(("input", "expected"),
    (List(0, 0), 0), (List(1), 1), (List(1, 3), 4),
    (List(1, 3, 5, 2, 1), 12), (List(1, 0, 5, 0), 6))

  property("a few pins") {
    score(simplePins)
  }

}
