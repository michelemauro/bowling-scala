# Principi del Kata

_Kata_ o "forma" indica, nelle arti marziali, la pratica consistente nell'esercitare le mosse dell'arte facendo la massima attenzione alla corretta esecuzione, al di fuori del contesto del combattimento. Lo scopo è raggiungere la massima padronanza dell'essenza della mossa stessa, appunto la sua "forma".

Nella programmazione, si chiama _Code Kata_ l'esercizio di risolvere un problema ponendo l'attenzione non sulla soluzione, ma sul percorso seguito per arrivarci e sull'esecuzione puntuale e rigorosa delle pratiche che si vogliono studiare.

# Specifiche dell'esercizio

Il problema su cui ci eserciteremo è il classico [Calcolo del punteggio del Bowling](http://codingdojo.org/cgi-bin/index.pl?KataBowling).

In breve, le regole sono le seguenti:

- il gioco del Bowling è diviso in Frames di uno o due tiri, tranne l'ultimo, il decimo, che può essere di tre tiri.
- se in un Frame non si abbattono tutti i birilli, il punteggio del frame è il totale dei birilli abbattuti
- se in un Frame si abbattono in due tiri tutti i birilli, il punteggio del frame è 10 più il punteggio del tiro successivo nel frame seguente.
- se in un Frame si abbattono tutti i birilli al primo tiro, il frame è concluso ed il suo punteggio è 10 più il punteggio dei due tiri successivi (nel o nei frame seguenti).
- nell'ultimo Frame, se si abbattono tutti e dieci i birilli al primo tiro o con il secondo, si ha diritto ad un terzo tiro.

Spesso, la trascrizione della sequenza dei tiri usa il carattere **-** per indicare zero birilli abbattuti, **/** per indicare che il secondo tiro ha abbattuto tutti i birilli rimanenti (_spare_), e **X** per indicare l'abbattimento di tutti i birilli con un tiro solo (_strike_).

# Specifiche tecniche

I test sono scritti usando le **PropSpec** di [ScalaTest](http://www.scalatest.org/). In questo modo le coppie (_valore di ingresso_, _risultato atteso_) possono essere scritte come _Table_ che vengono valutate da ciascun test (cfr: [Property-based testing](http://www.scalatest.org/user_guide/property_based_testing)). Formarvi e discutere la vostra opinione su questo modo di scrivere i test è parte integrante dell'esercizio.

Il progetto comprende un file **build.sbt** che lo definisce per il tool di build **SBT** e dichiara le dipendenze necessarie e che permette di predisporre l'ambiente di lavoro su Eclipse ScalaIde. Altri ambienti di sviluppo non dovrebbero aver problemi a riconoscere e configurare quanto necessario. Da linea di comando, si possono eseguire i test con **sbt test**. 

La classe che contiene i test, **BowlingTest** si aspetta che la classe sottoposta al test si chiami **BowlingGame** e abbia un metodo:

- `def score(input: List[Int]): Int`

I dati di ciascun test vengono passati al metodo opportuno ed il risultato confrontato con le aspettative. Ogni test è costituito da una **Table** di coppie (_valore di ingresso_, _risultato atteso_) e da una **property** che le verifica.

# Svolgimento dell'esercizio

**Prima dell'esercizio** è consigliato risolvere il problema autonomamente, per farsi un'idea generale e quindi avere, durante il Kata, più tempo a disposizione per riflettere sul metodo e sui passi seguiti nella costruzione della soluzione.

Per farlo è sufficiente clonare questo repository e posizionarsi direttamente al passo 6 con il comando

`git merge Step6`

A questo punto si può sviluppare la classe **BowlingGame** e lavorare per soddisfare tutti i test.

**Durante l'esercizio** il repository verrà aggiornato di passo in passo per mantenere tutti sullo stesso argomento. Quando verrà comunicato la presenza di un nuovo aggiornamento, lo si potrà ottenere con i comandi:

`git pull`
`git merge origin/step_<N>`

Essendo il contenuto di ciascun branch essenzialmente modifiche alla classe di test, il proprio codice non verrà toccato.

# Sequenza dei test

I test sono costruiti in questa sequenza, per indirizzare in una precisa direzione la costruzione della soluzione:

- passo 1: alcuni tiri come sequenza di interi
